import React from 'react';
import {
  withStyles,
  Card,
  CardContent,
  Typography,
  Button,
} from '@material-ui/core';
import { Styles } from './CustomCard.style';
import moment from 'moment';

const CustomCard = ({
  classes,
  id,
  fullName,
  companyName,
  title,
  checkedIn,
  checkedOut,
  handleClick,
  Selected,
  handleCheckOut,
}) => {
  //if the variables has value, it will return value else it will return 'N/A'
  fullName = fullName || 'N/A';
  companyName = companyName || 'N/A';
  title = title || 'N/A';
  checkedIn = checkedIn ? moment(checkedIn).format('MM/DD/YYYY, HH:mm') : 'N/A';
  checkedOut = checkedOut
    ? moment(checkedOut).format('MM/DD/YYYY, HH:mm')
    : 'N/A';
  //Checking current id is inside the Selected array
  let peopleChecked = Selected.includes(id);

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="h5" component="h2">
          {fullName}
        </Typography>
        <Typography className={classes.pos} color="textSecondary">
          Title: {title}
        </Typography>
        <Typography variant="body2" component="p">
          Company: {companyName}
        </Typography>
        <div className={classes.checking}>
          <Typography variant="body2" component="p">
            Checked in at: {checkedIn}
          </Typography>
          <Typography variant="body2" component="p">
            Checked out at: {checkedOut}
          </Typography>
        </div>
        {peopleChecked ? (
          <Button
            variant="contained"
            color="secondary"
            className={classes.button}
            onClick={() => handleCheckOut({ id, fullName, companyName })}
          >
            Check-out <br /> person {fullName}
          </Button>
        ) : (
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={() => handleClick({ id, fullName, companyName })}
          >
            Check-in <br /> person {fullName}
          </Button>
        )}
      </CardContent>
    </Card>
  );
};

export default withStyles(Styles)(CustomCard);
