export const Styles = {
  root: {
    textAlign: 'center',
  },
  checking: {
    marginTop: 5,
    borderTop: '1px solid #ccc',
    paddingTop: 5,
  },
  button: {
    marginTop: 10,
  },
};
