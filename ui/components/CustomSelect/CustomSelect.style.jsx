export const Styles = {
  formControl: {
    width: '350px',
    margin: '0 auto',
    display: 'block',
  },
  select: {
    width: '100%',
    textAlign: 'center',
  },
};
