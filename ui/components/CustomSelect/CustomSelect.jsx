import React from 'react';
import {
  withStyles,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from '@material-ui/core';
import { Styles } from './CustomSelect.style';

const CustomSelect = ({ classes, label, items, handleSelect, selected }) => {
  return (
    <FormControl className={classes.formControl}>
      <InputLabel id="select-label">{label}</InputLabel>
      <Select
        labelId="select-label"
        id="simple-select"
        className={classes.select}
        value={selected}
        onChange={handleSelect}
      >
        {items &&
          items.map(item => (
            <MenuItem key={item._id} value={item._id}>
              {item.name}
            </MenuItem>
          ))}
      </Select>
    </FormControl>
  );
};

export default withStyles(Styles)(CustomSelect);
