import React from 'react';
import { withStyles, Container, Typography } from '@material-ui/core';
import { TEXTS } from '../shared/constants';
import HomePage from './views/HomePage/HomePage';
import { Styles } from './App.style';

const App = ({ classes }) => (
  <Container>
    <Typography
      variant="h2"
      align="center"
      color="primary"
      className={classes.title}
    >
      {TEXTS.HOME_TITLE}
    </Typography>
    <HomePage />
  </Container>
);

export default withStyles(Styles)(App);
