import React, { Component } from 'react';
import { withStyles, Grid, Typography } from '@material-ui/core';
import { Styles } from './HomePage.style';
import { withTracker } from 'meteor/react-meteor-data';
import { Communities } from '../../../collections/communities';
import CustomSelect from '../../components/CustomSelect/CustomSelect';
import EventDetails from './EventDetails/EventDetails';
import ListContainer from './ListContainer/ListContainer';

class HomePage extends Component {
  state = {
    Community: '',
    Checked: [],
    RegPeople: [],
    Selected: [],
  };

  handleSelect = e => {
    this.setState({
      Community: e.target.value,
    });
  };

  //This function will call, once the user clicked check-in button
  handleClick = data => {
    //An array to store the list of checked in people
    let Checked = [...this.state.Checked, data];
    this.setState({
      Checked,
    });

    //To add Checked-in time to the people details
    this.setState({
      RegPeople: this.state.RegPeople.map(people => {
        if (data.id === people._id) {
          return {
            checkedIn: new Date(),
            ...people,
          };
        }
        return people;
      }),
    });
    //To update the button to check-out after 5 seconds
    setTimeout(() => {
      let Selected = [...this.state.Selected, data.id];
      this.setState({
        Selected,
      });
    }, 5000);
  };

  //This function will call, Once the user clicked the check-out button
  handleCheckOut = data => {
    //Removing an specific item from the Checked state
    let Checked = this.state.Checked.filter(check => check.id !== data.id);
    this.setState({
      Checked,
    });

    //To add Checked-out time to the people details
    this.setState({
      RegPeople: this.state.RegPeople.map(people => {
        if (data.id === people._id) {
          return {
            checkedOut: new Date(),
            ...people,
          };
        }
        return people;
      }),
    });

    //To remove the check-out details from the button
    let Selected = this.state.Selected.filter(check => check !== data.id);
    this.setState({
      Selected,
    });
  };

  //Getting the list of registered people, if the user selected an event and storing in the RegPeople state
  componentDidUpdate(prevProps, prevState) {
    if (this.state.Community !== prevState.Community) {
      setTimeout(() => {
        let people = JSON.parse(localStorage.getItem('lists'));
        this.setState({
          RegPeople: people,
        });
      }, 500);
    }
  }

  /*
  - I didn't filter the people without company in the below function.
  - To show the count of them the people without company, their company name will display as N/A 
  */

  //Function to get count of people in the event by company with company name
  toGetCompanyWithValue = arr => {
    //Mapping company name to get the array of only company names
    let companies = arr && arr.map(a => a.companyName);

    //Mapping company to get the array of comapny name with the count of people
    companies = companies.map(company => {
      if (!company) return;
      //Filtering the array of comapnies with current company
      let val = companies && companies.filter(comp => comp === company);
      return `${company} (${val.length})`;
    });
    return [...new Set(companies)]; //To remove duplicates company name and returning it
  };

  render() {
    const { classes, communities } = this.props;
    const { Community, Checked, RegPeople, Selected } = this.state;

    //To get details of selected event
    let selectedEvent = communities.find(
      community => community._id === Community
    );
    let peopleInTheEvent = Checked && Checked.length; //To get the count of total people in the event

    // To get the count of Registerd people, who are not checked in
    let RegPeopleCount = RegPeople && RegPeople.length;
    let CheckedCount = Checked && Checked.length;
    let peopleNotCheckedIn = RegPeopleCount - CheckedCount;

    return (
      <Grid
        container
        className={classes.homeWrapper}
        direction="column"
        justify="center"
        alignItems="center"
      >
        <Grid item sm={6}>
          <Typography
            color="primary"
            align="center"
            variant="h5"
            className={classes.title}
          >
            Select an event below for its details and to Check-in people
          </Typography>
          <CustomSelect
            items={communities}
            label="Select an event"
            selected={Community}
            handleSelect={this.handleSelect}
          />
        </Grid>
        {Community && (
          <>
            <Grid item sm={6}>
              <EventDetails
                people={peopleInTheEvent}
                selectedEventName={selectedEvent && selectedEvent.name}
                companies={this.toGetCompanyWithValue(Checked)}
                notChecked={peopleNotCheckedIn}
              />
            </Grid>
            <Grid item sm={12}>
              <ListContainer
                community={Community}
                RegPeople={RegPeople}
                handleCheckOut={this.handleCheckOut}
                handleClick={this.handleClick}
                Selected={Selected}
              />
            </Grid>
          </>
        )}
      </Grid>
    );
  }
}

export default withTracker(() => {
  //Subscribing to the collections to get the Communities details
  const handle = Meteor.subscribe('Communities');
  return {
    loading: !handle.ready(),
    communities: Communities.find().fetch(),
  };
})(withStyles(Styles)(HomePage));
