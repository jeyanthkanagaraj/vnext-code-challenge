import React, { Component } from 'react';
import CustomCard from '../../../../components/CustomCard/CustomCard';
import { withStyles, Grid, CircularProgress } from '@material-ui/core';
import { Styles } from './List.style';

class List extends Component {
  render() {
    const {
      classes,
      handleClick,
      Selected,
      RegPeople,
      handleCheckOut,
      loading,
    } = this.props;

    return (
      <Grid container className={classes.list} spacing={3}>
        {loading && <CircularProgress />}
        {RegPeople &&
          RegPeople.map(p => (
            <Grid item xs={3} key={p._id}>
              <CustomCard
                id={p._id}
                fullName={`${p.firstName} ${p.lastName}`}
                companyName={p.companyName}
                title={p.title}
                handleClick={handleClick}
                handleCheckOut={handleCheckOut}
                Selected={Selected}
                checkedIn={p.checkedIn && p.checkedIn}
                checkedOut={p.checkedOut && p.checkedOut}
              />
            </Grid>
          ))}
      </Grid>
    );
  }
}

export default withStyles(Styles)(List);
