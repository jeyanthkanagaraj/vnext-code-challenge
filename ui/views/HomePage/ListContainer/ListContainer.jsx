import { Meteor } from 'meteor/meteor';
import { People } from '../../../../collections/people';
import { withTracker } from 'meteor/react-meteor-data';
import List from './List/List';

//Subscribing to the collections to get selected event people details
const ListPageContainer = withTracker(
  ({ community, handleClick, Selected, RegPeople, handleCheckOut }) => {
    const handle = Meteor.subscribe('People', community);
    const people = People.find({ communityId: community }).fetch();
    //Storing the people details in localStorage
    localStorage.setItem('lists', JSON.stringify(people));
    return {
      loading: !handle.ready(),
      people,
    };
  }
)(List);

export default ListPageContainer;
