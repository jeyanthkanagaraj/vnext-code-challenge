export const Styles = {
  eventDetails: {
    marginTop: 30,
    padding: 20,
    '& span': {
      color: '#3f51b5',
    },
  },
};
