import React from 'react';
import { withStyles, Typography, Paper } from '@material-ui/core';
import { Styles } from './EventDetails.style';

const EventDetails = ({
  classes,
  people,
  companies,
  notChecked,
  selectedEventName,
}) => {
  //To add Comma after each company and to remove the comma from last company
  let company = companies.length
    ? companies.map(company => {
        if (companies.indexOf(company) === companies.length - 1) {
          return company;
        }
        return `${company}, `;
      })
    : 0;
  return (
    <Paper className={classes.eventDetails} elevation={3}>
      <Typography
        variant="h4"
        className={classes.title}
        align="center"
        color="primary"
      >
        {selectedEventName} Event Details
      </Typography>
      <Typography align="center">
        People in the event right now: <span>{people}</span>
      </Typography>
      <Typography align="center">
        People by company in the event right now: <span>{company}</span>
      </Typography>
      <Typography align="center">
        People not checked-in: <span>{notChecked}</span>
      </Typography>
    </Paper>
  );
};

export default withStyles(Styles)(EventDetails);
